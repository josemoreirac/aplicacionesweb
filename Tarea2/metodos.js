var inputs = document.getElementsByClassName('formulario__input');

for (var i = 0; i < inputs.length; i++){
    inputs[i].addEventListener('keyup', function(){
        if (this.value.length >= 1){
            this.nextElementSibling.classList.add('fijar');
        } else {
            this.nextElementSibling.classList.remove('fijar');
        }
    });
}



var pattern = /\d/,
    caja = document.getElementById("codigo");
 
caja.addEventListener("keypress", function(e){
    if (!pattern.test(String.fromCharCode(e.keyCode)) || this.value.length == 11)
        e.preventDefault();
    if (this.value.length === 9)
        this.value += "-";
}, false);