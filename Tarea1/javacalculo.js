function Horas(){
    const def = 3600;
    var hora = new Date().getHours();
    var minutos = new Date().getMinutes();
    var segundos = new Date().getSeconds();
    
    document.getElementById("fecha").innerHTML=hora + ":"+minutos+":"+segundos +"<br>";
    
    var conversion = hora * def;
    
    document.getElementById("hola").innerHTML = "Horas a segundos: <br>"+conversion;
}

document.getElementById("boton").addEventListener(('click'), (e)=>{

    e.preventDefault();
    var base = document.getElementById("base").value;
    var altura = document.getElementById("altura").value;

    var resul = (base * altura)/2;

    document.getElementById('res').innerHTML = "El area es:  "+resul;
})

document.getElementById("boton-raiz").addEventListener(("click"), (e)=>{
    e.preventDefault();

    var raiz = document.getElementById("raiz").value;
    var resul_raiz = Math.sqrt(raiz);

    document.getElementById("result").innerHTML = resul_raiz.toPrecision(3);
    
})

document.getElementById("boton-palabra").addEventListener(("click"), ()=>{

    var palabra = document.getElementById("palabra").value;

    document.getElementById("result-palabra").innerHTML = palabra.length;

})
var array1 = ["Lunes", "Martes", "Miercoles", "Jueves", "Viernes","Sabado", "Domingo"];


document.getElementById("concatenar").innerHTML = array1 ;

document.getElementById("version-navegador").innerHTML = navigator.appVersion;
document.getElementById("tamaño").innerHTML = "Ancho "+screen.width +" alto "+ screen.height;
document.getElementById("impresion").addEventListener(("click"), (e)=>{

    e.preventDefault();
    window.print();

})

Horas();
setInterval(Horas, 1000);