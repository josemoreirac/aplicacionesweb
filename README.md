function guardarLibros() {
  // obtener los valores de los inputs

  let codigo = document.getElementById("codigo").value;
  let titulo = document.getElementById("titulo").value;
  let autor = document.getElementById("autor").value;
  let editorial = document.getElementById("editorial").value;
  let anio = document.getElementById("anio").value;

  // valida campos vacios
  if (
    codigo.length == 0 ||
    titulo.length == 0 ||
    autor.length == 0 ||
    editorial.length == 0 ||
    anio.length == 0
  ) {
    // si algun campo esta vacio se muestra la siguiente alerta
    alert("campos vacios");
  } else {
    // por el contrario se crea el objeto en formato JSON

    let datos = {
      codigo: codigo,
      titulo: titulo,
      autor: autor,
      editorial: editorial,
      anio: anio,
    };

    localStorage.setItem("libros", JSON.stringify(datos));
  }
}







function guardarLibrosConValidacion() {
  // obtener los valores de los inputs

  let codigo = document.getElementById("codigo").value;
  let titulo = document.getElementById("titulo").value;
  let autor = document.getElementById("autor").value;
  let editorial = document.getElementById("editorial").value;
  let anio = document.getElementById("anio").value;

  // valida campos vacios
  if (
    codigo.length == 0 ||
    titulo.length == 0 ||
    autor.length == 0 ||
    editorial.length == 0 ||
    anio.length == 0
  ) {
    // si algun campo esta vacio se muestra la siguiente alerta
    alert("campos vacios");
  } else {
    // validar longitud y datos alfanumericos y numericos

    if (
      codigo.length <= 5 &&
      titulo.length <= 100 &&
      autor.length <= 60 &&
      editorial.length <= 30 &&
      anio.length <= 4
    ) {
      //validar numero
      if (isNaN(anio) == false) {
        // por el contrario se crea el objeto en formato JSON

        let datos = {
          codigo: codigo,
          titulo: titulo,
          autor: autor,
          editorial: editorial,
          anio: anio,
        };

        localStorage.setItem("libros", JSON.stringify(datos));
      } else {
        alert("El campo año solo debe tener numeros");
      }
    } else {
      // si algun campo esta vacio se muestra la siguiente alerta
      alert("No cumple con las longitudes establecidad");
    }
  }
}
